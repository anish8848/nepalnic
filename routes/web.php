<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FrontEndController@home')->name('new_home');
Route::get('/about-us','FrontEndController@about_us')->name('about-us');
Route::get('/contact-us','FrontEndController@contact')->name('contact');
Route::get('/our-services','FrontEndController@service')->name('our-services');
Route::get('/media','FrontEndController@media')->name('media');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
