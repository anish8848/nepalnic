<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from codecares.com/html/bizzhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Nov 2018 04:56:18 GMT -->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nepal NIC | Business And Corporate</title>

    <!-- Favicon -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Preloader -->
    <div id="preloader"></div>



    <!-- Main Header start -->
    <header class="main-herader">
        <!-- Header topbar start -->
        <div class="header-topbar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-sm-6 col-xs-12">
                        <div class="herader-topbar-col tobar-leftside center767">
                            <p>Welcome To Nepal NIC</p>
                        </div>
                    </div>
                    <div class="col-lg-5 col-sm-6 col-xs-12">
                        <div class="herader-topbar-col tobar-rightside center767">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Header Middle -->
        <div class="header-middle">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12 full-wd480">
                        <div class="header-middle-col main-logo">
                           <a href="index-2.html"><img src="images/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-1 col-sm-4 col-xs-6 full-wd480">
                        <div class="header-middle-col my-info-box">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <h5>Mail</h5>
                            <p>info@nepalnic.com</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-6 full-wd480">
                        <div class="header-middle-col my-info-box">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <h5>Phone Number</h5>
                            <p>+0123 1205 1010</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12 full-wd480">
                        <div class="header-middle-col quote-box">
                            <a href="#quote" id="scroll">Get a quote</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Header navbar start -->
        <div class="header-navbar fixed-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="my-nav-row">
                            <div class="row">
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                    <nav class="navbar navbar-default">
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" data-hover="dropdown" data-animations="fadeIn">
                                            <ul class="nav navbar-nav">
                                                <li class="dropdown active">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                                    <!-- <ul class="dropdown-menu">
                                                        <li><a href="index-2.html">Home one</a></li>
                                                        <li><a href="index-two.html">Home two</a></li>
                                                        <li><a href="index-three.html">Home Three</a></li>
                                                    </ul> -->
                                                </li>
                                                <li><a href="about.html">About Us</a></li>
                                                <li><a href="service.html">Services</a></li>
                                                <!-- <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Portfolio <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="portfolio-one.html">Portfolio One</a></li>
                                                        <li><a href="portfolio-two.html">Portfolio Two</a></li>
                                                        <li><a href="portfolio-single.html">Portfolio Single</a></li>
                                                    </ul>
                                                </li> -->
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Media </a>
                                                    <!-- <ul class="dropdown-menu">
                                                        <li><a href="blog.html">Blog</a></li>
                                                        <li><a href="blog-right-sidebar.html">Blog right sidebar</a></li>
                                                        <li><a href="blog-left-sidebar.html">Blog left sidebar</a></li>
                                                        <li><a href="blog-single.html">Blog Single</a></li>
                                                    </ul> -->
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="team.html">FAQs</a></li>
                                                        <li><a href="team-single.html">Driver and Rider</a></li>
                                                        <li><a href="pricing.html">Career</a></li>
                                                        <!-- <li><a href="signin-signup.html">Sign in / Sign Up</a></li>
                                                        <li><a href="faq.html">Faq Page</a></li>
                                                        <li><a href="404.html">404 Page</a></li> -->
                                                    </ul>
                                                </li>
                                                <li><a href="contact.html">Contact</a></li>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="my-search-box">
                                        <form method="post">
                                            <div class="input-group">
                                                <input placeholder="Search Here....." class="form-control" name="search-field" type="text">
                                                <span class="input-group-btn">
                                              <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                              </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Slider Start -->
    <section class="main-slider-area">
        <div class="main-container">
            <div id="carousel-example-generic" class="carousel slide carousel-fade">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <!-- First slide -->
                    <div class="item active slide-1 text-center">
                        <div class="carousel-caption">
                            <p data-animation="animated fadeInUp">
                               Business And Corporate
                            </p>
                            <h3 data-animation="animated fadeInUp">
                                Welcome to our <span>Bizzhub</span>
                            </h3>
                            <a href="#" class="btn btn-primary btn-lg theme-btn" data-animation="animated zoomIn">Register Now</a>
                        </div>
                    </div>
                    <!-- /.item -->

                    <!-- Second slide -->
                    <div class="item slide-2 text-right">
                        <div class="carousel-caption">
                            <p data-animation="animated fadeInUp">
                                Business And Corporate
                            </p>
                            <h3 data-animation="animated fadeInUp">
                                We are very <span>trusted</span>
                            </h3>
                            <a href="#" class="btn btn-primary btn-lg theme-btn" data-animation="animated zoomIn">Register Now</a>
                        </div>
                    </div>
                    <!-- /.item -->

                    <!-- Third slide -->
                    <div class="item slide-3 text-left">
                        <div class="carousel-caption">
                            <p data-animation="animated fadeInUp">
                               Business And Corporate
                            </p>
                            <h3 data-animation="animated fadeInUp">
                                start your <span>business</span> today
                            </h3>
                            <a href="#" class="btn btn-primary btn-lg theme-btn" data-animation="animated zoomIn">Register Now</a>
                        </div>
                    </div>
                    <!-- /.item -->

                </div>
                <!-- /.carousel-inner -->

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- /.carousel -->
        </div>
    </section>

    <!-- About Start -->
    <section class="about-two-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <div class="about-col">
                        <h2>Message from <span>Chairman</span></h2>
                        <h4>More than 12 years of experience</h4>
                        <p>Morbi scelerisque volutpat egestas. Fusce dapibus rutrum magna, id pharetra lectus consectetur quis. Nunc ut porta enim, ac vulputate nisl. Vivamus sit amet dui quis leo suscipit scelerisque. Suspendisse euismod magna nec justo aliquam, tincidunt luctus mauris ultricies. Sed pellentesque, ligula at lacinia molestie, nisl sapien consequat nibh, in mattis dolor dolor ac lorem. Sed in molestie lectus. Quisque hendrerit.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem architecto debitis sapiente alias adipisci doloremque, magnam nihil accusamus cum suscipit, placeat optio tempore, laborum sit ab esse. Magnam, necessitatibus, accusantium.Morbi scelerisque volutpat egestas. Fusce dapibus rutrum magna, id pharetra lectus consectetur quis. Nunc ut porta enim, ac vulputate nisl. Vivamus sit amet dui quis leo suscipit scelerisque. </p>
                        <div class="row">
                            <div class="col-xs-4 fw600">

                            </div>
                            <div class="col-xs-4 fw600">

                            </div>
                            <div class="col-xs-4 fw600">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5">
                    <div class="about-img-col">
                        <img src="images/about/1.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>














    <!-- Testimonial Start -->
    <section class="testimonial-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>testimonial</h2>
                        <div class="title-border"></div>
                        <p>Sed pellentesque, ligula at lacinia molestie sapien consequat</p>
                    </div>
                </div>
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="testimonial-carousel">
                        <div class="testimonial">
                            <div class="pic">
                                <img src="images/testimonial/1.jpg" alt="" class="img-responsive">
                            </div>
                            <h3 class="testimonial-title">
                                Fokir Baba
                                <small>CEO, Envato</small>
                            </h3>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi facilisis.</p>
                        </div>
                        <div class="testimonial">
                            <div class="pic">
                                <img src="images/testimonial/2.jpg" alt="" class="img-responsive">
                            </div>
                            <h3 class="testimonial-title">
                                Pairlal
                                <small>Manager, Envato</small>
                            </h3>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi facilisis.</p>
                        </div>
                        <div class="testimonial">
                            <div class="pic">
                                <img src="images/testimonial/3.jpg" alt="" class="img-responsive">
                            </div>
                            <h3 class="testimonial-title">
                                Ananta Jalil
                                <small>Web Developer, Envato</small>
                            </h3>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi facilisis.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Client start -->
    <section class="client-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="client-carousel">
                        <div class="item">
                            <a href="#"><img src="images/client/1.jpg" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="images/client/2.jpg" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="images/client/3.jpg" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="images/client/4.jpg" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="images/client/5.jpg" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="images/client/6.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer start -->
    <section class="our-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="our-footer-col">
                        <div class="footer-title">
                            <h2>Recent post</h2>
                        </div>
                        <div class="our-post">
                            <img src="images/blog/post-1.jpg" alt="">
                            <p><a href="#">Consectetur adipisicing elit. Eveniet, ex quis atque ab est corporis.</a></p>
                            <a href="#">3 minutes ago</a>
                        </div>
                        <div class="our-post">
                            <img src="images/blog/post-2.jpg" alt="">
                            <p><a href="#">Consectetur adipisicing elit. Eveniet, ex quis atque ab est corporis.</a></p>
                            <a href="#">5 minutes ago</a>
                        </div>
                        <div class="our-post last-post">
                            <img src="images/blog/post-3.jpg" alt="">
                            <p><a href="#">Consectetur adipisicing elit. Eveniet, ex quis atque ab est corporis.</a></p>
                            <a href="#">10 minutes ago</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="our-footer-col">
                        <div class="our-footer-logo">
                            <a href="#"><img src="images/logo-2.png" alt=""></a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed numquam hic nobis! Sint accusamus sapiente excepturi debitis corporis.</p>
                            <ul class="our-footer-social clearfix">
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="subscribe-area">
                            <div class="footer-title">
                                <h2>Subscribe for Newsletter</h2>
                            </div>
                            <form>
                                <div class="input-group">
                                    <input placeholder="Email Address" class="form-control" name="search-field" type="text">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-hover">Subscribe</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="our-footer-col clearfix">
                        <div class="footer-title">
                            <h2>Quick contact</h2>
                        </div>
                        <div class="quick-contact">
                            <form id="ajax-contact" method="post" action="http://codecares.com/html/bizzhub/php/contact.php">
                                <div class="row">
                                    <div class="col-sm-6">
                                       <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required="">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="">
                                    </div>
                                    <div class="col-sm-12">
                                        <textarea class="form-control textarea-hight-full" id="message" name="message" rows="6" placeholder="Message" required=""></textarea>
                                        <button class="btn btn-default theme-btn btn-hover" type="submit">Sent Message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Copyright start from here -->
    <div class="copyright">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright-col text-center">
                    <p>Copyright ©2018 <a href="https://themeforest.net/user/xcodesolution" target="_blank">xcodesolutio</a> All Rights Reserved</p>
                </div>
            </div>
        </div>
    </div>


           <!-- modernizr -->
           <script src="js/modernizr-2.6.2.min.js"></script>

           <!-- jQuery -->
           <script src="js/jquery.min.js"></script>

           <!-- Bootstrap Core JavaScript -->
           <script src="js/bootstrap.min.js"></script>

           <!-- All Included JavaScript -->
           <script src="js/bootstrap-dropdownhover.min.js"></script>
           <script src="js/jquery-scrolltofixed-min.js"></script>
           <script src="js/owl.carousel.min.js"></script>
           <script src="js/jarallax.min.js"></script>
           <script src="js/jquery.countup.min.js"></script>
           <script src="js/jquery.waypoints.min.js"></script>
           <script src="js/jquery.ripples.js"></script>
           <script src="js/dyscrollup.js"></script>
           <script src="js/VideoPlayerPopUp.js"></script>
           <script src="js/animated-text.js"></script>
           <script src="js/jquery.zoomslider.min.js"></script>
           <script src="js/YouTubePopUp.jquery.js"></script>
           <script src="js/lightbox.min.js"></script>
           <script src="js/imagesloaded.min.js"></script>
           <script src="js/jquery.filterizr.min.js"></script>

           <!-- Custom Js -->
           <script src="js/main.js"></script>


       </body>


       <!-- Mirrored from codecares.com/html/bizzhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Nov 2018 04:57:46 GMT -->
       </html>
