
    <!-- Preloader -->
    <div id="preloader"></div>



    <!-- Main Header start -->
    <header class="main-herader">
        <!-- Header topbar start -->
        <!-- <div class="header-topbar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-sm-6 col-xs-12">
                        <div class="herader-topbar-col tobar-leftside center767">
                          <marquee>  <p>Welcome To Nepal NIC</p></marquee>
                        </div>
                    </div>
                    <div class="col-lg-5 col-sm-6 col-xs-12">
                        <div class="herader-topbar-col tobar-rightside center767">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        <!-- Header Middle -->
        <div class="header-middle">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12 full-wd480">
                        <div class="header-middle-col main-logo">
                           <a href="{{route('new_home')}}"><img src="images/logo1.png" alt=""></a>
                        </div>
                    </div>
                    <!-- <div class="col-md-3 col-md-offset-1 col-sm-4 col-xs-6 full-wd480">
                        <div class="header-middle-col my-info-box">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <h4>Mail</h4>
                            <p>info@nepalnic.com</p>
                        </div>
                    </div> -->
                    <!-- <div class="col-md-3 col-sm-4 col-xs-6 full-wd480">
                        <div class="header-middle-col my-info-box">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <h4>Phone Number</h4>
                            <p>+0123 1205 1010</p>
                        </div>
                    </div> -->
                    <!-- <div class="col-md-2 col-sm-12 col-xs-12 full-wd480">
                        <div class="header-middle-col quote-box">
                            <a href="#quote" id="scroll">Get a quote</a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

        <!-- Header navbar start -->
        <div class="header-navbar fixed-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="my-nav-row">
                            <div class="row">
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                    <nav class="navbar navbar-default">
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" data-hover="dropdown" data-animations="fadeIn">
                                            <ul class="nav navbar-nav">
                                                  <li><a href="{{route('new_home')}}">Home</a></li>
                                                <!-- <li>
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home</a> -->
                                                    <!-- <ul class="dropdown-menu">
                                                        <li><a href="index-2.html">Home one</a></li>
                                                        <li><a href="index-two.html">Home two</a></li>
                                                        <li><a href="index-three.html">Home Three</a></li>
                                                    </ul> -->
                                                <!-- </li> -->
                                                <li><a href="{{route('about-us')}}">About Us</a></li>
                                                <li><a href="{{route('our-services')}}">Services</a></li>
                                                <!-- <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Portfolio <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="portfolio-one.html">Portfolio One</a></li>
                                                        <li><a href="portfolio-two.html">Portfolio Two</a></li>
                                                        <li><a href="portfolio-single.html">Portfolio Single</a></li>
                                                    </ul>
                                                </li> -->
                                                  <li><a href="{{route('media')}}">Media</a></li>
                                                <!-- <li class="dropdown">
                                                    <a href="{{route('media')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Media </a> -->
                                                    <!-- <ul class="dropdown-menu">
                                                        <li><a href="blog.html">Blog</a></li>
                                                        <li><a href="blog-right-sidebar.html">Blog right sidebar</a></li>
                                                        <li><a href="blog-left-sidebar.html">Blog left sidebar</a></li>
                                                        <li><a href="blog-single.html">Blog Single</a></li>
                                                    </ul> -->
                                                  <!-- </li> -->

                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="team.html">Terms and Conditions</a></li>
                                                        <li><a href="team-single.html">Privacy Policy</a></li>
                                                        <li><a href="pricing.html">FAQ'S</a></li>
                                                        <!-- <li><a href="signin-signup.html">Sign in / Sign Up</a></li>
                                                        <li><a href="faq.html">Faq Page</a></li>
                                                        <li><a href="404.html">404 Page</a></li> -->

                                                    </ul>
                                                </li>
                                                <li><a href="#">career</a></li>
                                                <li><a href="{{route('contact')}}">Contact</a></li>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="my-search-box">
                                        <form method="post">
                                            <div class="input-group">
                                                <input placeholder="Search Here....." class="form-control" name="search-field" type="text">
                                                <span class="input-group-btn">
                                              <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                              </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
