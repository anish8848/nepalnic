<!-- About Start -->
<section class="about-two-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7">
                <div class="about-col">
                    <h2>Message from <span>Chairman</span></h2>
                    <h4>More than 12 years of experience</h4>
                    <p>Morbi scelerisque volutpat egestas. Fusce dapibus rutrum magna, id pharetra lectus consectetur quis. Nunc ut porta enim, ac vulputate nisl. Vivamus sit amet dui quis leo suscipit scelerisque. Suspendisse euismod magna nec justo aliquam, tincidunt luctus mauris ultricies. Sed pellentesque, ligula at lacinia molestie, nisl sapien consequat nibh, in mattis dolor dolor ac lorem. Sed in molestie lectus. Quisque hendrerit.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem architecto debitis sapiente alias adipisci doloremque, magnam nihil accusamus cum suscipit, placeat optio tempore, laborum sit ab esse. Magnam, necessitatibus, accusantium.Morbi scelerisque volutpat egestas. Fusce dapibus rutrum magna, id pharetra lectus consectetur quis. Nunc ut porta enim, ac vulputate nisl. Vivamus sit amet dui quis leo suscipit scelerisque. </p>
                    <div class="row">
                        <div class="col-xs-4 fw600">

                        </div>
                        <div class="col-xs-4 fw600">

                        </div>
                        <div class="col-xs-4 fw600">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5">
                <div class="about-img-col">
                    <img src="images/about/1.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
