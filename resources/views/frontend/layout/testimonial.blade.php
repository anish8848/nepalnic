<!-- Testimonial Start -->
<section class="testimonial-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2>testimonial</h2>
                    <div class="title-border"></div>
                    <p>Sed pellentesque, ligula at lacinia molestie sapien consequat</p>
                </div>
            </div>
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="testimonial-carousel">
                    <div class="testimonial">
                        <div class="pic">
                            <img src="images/testimonial/1.jpg" alt="" class="img-responsive">
                        </div>
                        <h3 class="testimonial-title">
                            Fokir Baba
                            <small>CEO, Envato</small>
                        </h3>
                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi facilisis.</p>
                    </div>
                    <div class="testimonial">
                        <div class="pic">
                            <img src="images/testimonial/2.jpg" alt="" class="img-responsive">
                        </div>
                        <h3 class="testimonial-title">
                            Pairlal
                            <small>Manager, Envato</small>
                        </h3>
                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi facilisis.</p>
                    </div>
                    <div class="testimonial">
                        <div class="pic">
                            <img src="images/testimonial/3.jpg" alt="" class="img-responsive">
                        </div>
                        <h3 class="testimonial-title">
                            Ananta Jalil
                            <small>Web Developer, Envato</small>
                        </h3>
                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi facilisis.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
