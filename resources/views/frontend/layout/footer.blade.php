

<!-- Copyright start from here -->
<div class="copyright">
    <div class="row">
        <div class="col-md-12">
            <div class="copyright-col text-center">
                <p>Copyright ©2018 <a href="{{route('new_home')}}" target="_blank">NepalNIC</a> All Rights Reserved</p>
            </div>
        </div>
    </div>
</div>
