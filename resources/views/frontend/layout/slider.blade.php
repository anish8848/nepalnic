
    <!-- Main Slider Start -->
    <section class="main-slider-area">
        <div class="main-container">
            <div id="carousel-example-generic" class="carousel slide carousel-fade">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <!-- First slide -->
                    <div class="item active slide-1 text-center">
                        <div class="carousel-caption">
                            <p data-animation="animated fadeInUp">
                               Business And Corporate
                            </p>
                            <h3 data-animation="animated fadeInUp">
                                Welcome to Nepal<span>NIC</span>
                            </h3>
                            <a href="#" class="btn btn-primary btn-lg theme-btn" data-animation="animated zoomIn">Register Now</a>
                        </div>
                    </div>
                    <!-- /.item -->

                    <!-- Second slide -->
                    <div class="item slide-2 text-right">
                        <div class="carousel-caption">
                            <p data-animation="animated fadeInUp">
                                Business And Corporate
                            </p>
                            <h3 data-animation="animated fadeInUp">
                                We are very <span>trusted</span>
                            </h3>
                            <a href="#" class="btn btn-primary btn-lg theme-btn" data-animation="animated zoomIn">Register Now</a>
                        </div>
                    </div>
                    <!-- /.item -->

                    <!-- Third slide -->
                    <div class="item slide-3 text-left">
                        <div class="carousel-caption">
                            <p data-animation="animated fadeInUp">
                               Business And Corporate
                            </p>
                            <h3 data-animation="animated fadeInUp">
                                start your <span>business</span> today
                            </h3>
                            <a href="#" class="btn btn-primary btn-lg theme-btn" data-animation="animated zoomIn">Register Now</a>
                        </div>
                    </div>
                    <!-- /.item -->

                </div>
                <!-- /.carousel-inner -->

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- /.carousel -->
        </div>
    </section>
