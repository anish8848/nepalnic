<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from codecares.com/html/bizzhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Nov 2018 04:56:18 GMT -->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nepal NIC | Business And Corporate</title>

    <!-- Favicon -->
    <link href="{{ asset('images/logo1.png') }}" rel="shortcut icon" type="image/png') }}">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">





    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

@stack('stylesheet')

</head>

<body>

@include('frontend.layout.header')



@yield('content')



@include('frontend.layout.footer')


  <!-- modernizr -->
  <script src="{{ asset('js/modernizr-2.6.2.min.js') }}"></script>

  <!-- jQuery -->
  <script src="{{ asset('js/jquery.min.js') }}"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>

  <!-- All Included JavaScript -->
  <script src="{{ asset('js/bootstrap-dropdownhover.min.js') }}"></script>
  <script src="{{ asset('js/jquery-scrolltofixed-min.js') }}"></script>
  <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('js/jarallax.min.js') }}"></script>
  <script src="{{ asset('js/jquery.countup.min.js') }}"></script>
  <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('js/jquery.ripples.js') }}"></script>
  <script src="{{ asset('js/dyscrollup.js') }}"></script>
  <script src="{{ asset('js/VideoPlayerPopUp.js') }}"></script>
  <script src="{{ asset('js/animated-text.js') }}"></script>
  <script src="{{ asset('js/jquery.zoomslider.min.js') }}"></script>
  <script src="{{ asset('js/YouTubePopUp.jquery.js') }}"></script>
  <script src="{{ asset('js/lightbox.min.js') }}"></script>
  <script src="{{ asset('js/imagesloaded.min.js') }}"></script>
  <script src="{{ asset('js/jquery.filterizr.min.js') }}"></script>

  <!-- Custom Js -->
  <script src="{{ asset('js/main.js') }}"></script>

@stack('script')
@stack('scripts')
 </body>


 <!-- Mirrored from codecares.com/html/bizzhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Nov 2018 04:57:46 GMT -->
 </html>
