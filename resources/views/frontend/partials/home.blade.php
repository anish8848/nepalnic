@extends('frontend.main')

@section('content')

@include('frontend.layout.slider')

@include('frontend.layout.message')

@include('frontend.layout.testimonial')

<!-- Client start -->
<section class="client-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="client-carousel">
                    <div class="item">
                        <a href="#"><img src="images/client/1.jpg" alt=""></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="images/client/2.jpg" alt=""></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="images/client/3.jpg" alt=""></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="images/client/4.jpg" alt=""></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="images/client/5.jpg" alt=""></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="images/client/6.jpg" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
