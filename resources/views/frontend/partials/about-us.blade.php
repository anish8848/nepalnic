@extends('frontend.main')

@section('content')

<!-- Page heading Start -->
    <section class="page-heading-area jarallax overlay-black" id="water-animation">
        <img class="jarallax-img" src="images/bg/4.jpg" alt="">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="page-heading-col border-hover">
                        <h2>About Us</h2>
                        <p><a href="index-2.html">Home</a> / <a href="#">About</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Inner About Start -->
    <section class="about-inner-area">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="about-inner-col">
                        <h2> Company Profile </h2>
                        <!-- <h4>Lorem ipsum dolor sit adipisicing.</h4> -->
                        <p class="mb30">Consectetur adipisicing elit. Id, ex, laborum! Numquam labore explicabo vel placeat iure, temporibus, ducimus itaque quis sapiente, recusandae, porro dignissimos doloribus! Consequatur, autem. Numquam, vero totam quae beatae cupiditate sint asperiores veniam obcaecati quaerat repellendus enim, reprehenderit ad, ullam ipsa voluptatibus iste deleniti vitae tempore, ex sequi quis adipisci error dolorum.</p>
                        <p>Alias voluptates, assumenda possimus modi quas maxime officiis temporibus accusantium voluptatum ipsam et. Eius optio, voluptatum, eveniet ab aperiam sequi. Nam rem, earum voluptatem blanditiis repellat tempora iste dolor.</p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="about-inner-col">
                        <img src="images/about/about.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row choose-row">
                <div class="col-md-5">
                    <div class="choose-col">
                        <img src="images/about/2.jpg" alt="">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="choose-col">
                        <h2>Mission and Vision</h2>
                        <p>Consectetur adipisicing elit. Id, ex, laborum! Numquam labore explicabo vel placeat iure, temporibus, ducimus itaque quis sapiente, recusandae, porro dignissimos doloribus! Consequatur, autem. Numquam, vero totam quae beatae cupiditate.</p>
                        <ul>
                            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> There are many variations of passages of Lorem Ipsum.</li>
                            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Lorem Ipsum is simply dummy text of the printing. </li>
                            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Lorem Ipsum is not simply random text</li>
                            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Lorem Ipsum is simply dummy text of the printing.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- <div class="team">

            </div> -->

    <!-- Team Start -->
    <section class="team-area">
        <div class="container">
            <div class="row">
                  <h2 align="center">Our Team</h2><br>
                <div class="col-md-3 col-sm-6 col-xs-6 fw600">
                    <div class="our-team">
                        <div class="pic">
                            <img src="images/team/1.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title"><a href="team-single.html">Montu Mia</a></h3>
                            <span class="post">CEO And Founder</span>
                            <ul class="social">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-skype"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 fw600">
                    <div class="our-team">
                        <div class="pic">
                            <img src="images/team/2.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title"><a href="team-single.html">Tunir Ma</a></h3>
                            <span class="post">Marketing Manager</span>
                            <ul class="social">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-skype"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 fw600">
                    <div class="our-team">
                        <div class="pic">
                            <img src="images/team/3.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title"><a href="team-single.html">Deldar</a></h3>
                            <span class="post">General Manager</span>
                            <ul class="social">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-skype"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 fw600">
                    <div class="our-team">
                        <div class="pic">
                            <img src="images/team/4.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title"><a href="team-single.html">Kiron Mala</a></h3>
                            <span class="post">Business Advisor</span>
                            <ul class="social">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-skype"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 fw600">
                    <div class="our-team">
                        <div class="pic">
                            <img src="images/team/5.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title"><a href="team-single.html">Ananta Jolil</a></h3>
                            <span class="post">Accountant</span>
                            <ul class="social">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-skype"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 fw600">
                    <div class="our-team">
                        <div class="pic">
                            <img src="images/team/6.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title"><a href="team-single.html">Maleka Vanu</a></h3>
                            <span class="post">Project manager</span>
                            <ul class="social">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-skype"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 fw600">
                    <div class="our-team">
                        <div class="pic">
                            <img src="images/team/7.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title"><a href="team-single.html">Teli Samad</a></h3>
                            <span class="post">marketing executive</span>
                            <ul class="social">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-skype"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 fw600">
                    <div class="our-team">
                        <div class="pic">
                            <img src="images/team/8.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title"><a href="team-single.html">Bilkiss Vanu</a></h3>
                            <span class="post">business consultant</span>
                            <ul class="social">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-skype"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <nav class="pagination-outer" aria-label="Page navigation">
                <ul class="pagination">
                    <li class="page-item">
                        <a href="#" class="page-link" aria-label="Previous">
                            <span aria-hidden="true">«</span>
                        </a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item">
                        <a href="#" class="page-link" aria-label="Next">
                            <span aria-hidden="true">»</span>
                        </a>
                    </li>
                </ul>
            </nav> -->
        </div>
    </section>
        </div>
    </section>

@endsection
