@extends('frontend.main')

@section('content')
<!-- Gallery Start -->
   <section class="gallery-area">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
<h2 style="text-align:center">Gallery</h2><br>
                   <!-- Filter Nav -->
                   <!-- <ul class="portfolio-nav">
                       <li data-filter="all"> All </li>
                       <li data-filter="1"> Design </li>
                       <li data-filter="2"> Development </li>
                       <li data-filter="3"> Mobile app </li>
                   </ul> -->
                   <div class="filtr-container">
                       <div class="col-md-4 filtr-item" data-category="3, 2" data-sort="value">
                           <div class="box">
                               <img src="images/gellary/1.jpg" alt="">
                               <div class="box-content">
                                   <h3 class="title">Photo One</h3>
                                   <ul class="icon">
                                       <li>
                                           <a href="images/gellary/1.jpg" data-lightbox="lightbox" data-title="Photo Title">
                                               <i class="fa fa-search"></i>
                                           </a>
                                       </li>
                                       <li><a href="#" class="fa fa-link"></a></li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                       <div class="col-md-4 filtr-item" data-category="2, 1" data-sort="value">
                           <div class="box">
                               <img src="images/gellary/2.jpg" alt="">
                               <div class="box-content">
                                   <h3 class="title">Photo Two</h3>
                                   <ul class="icon">
                                       <li>
                                           <a href="images/gellary/2.jpg" data-lightbox="lightbox" data-title="Photo Title">
                                               <i class="fa fa-search"></i>
                                           </a>
                                       </li>
                                       <li><a href="#" class="fa fa-link"></a></li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                       <div class="col-md-4 filtr-item" data-category="3, 1" data-sort="value">
                           <div class="box">
                               <img src="images/gellary/3.jpg" alt="">
                               <div class="box-content">
                                   <h3 class="title">Photo Three</h3>
                                   <ul class="icon">
                                       <li>
                                           <a href="images/gellary/3.jpg" data-lightbox="lightbox" data-title="Photo Title">
                                               <i class="fa fa-search"></i>
                                           </a>
                                       </li>
                                       <li><a href="#" class="fa fa-link"></a></li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                       <div class="col-md-4 filtr-item" data-category="1, 2" data-sort="value">
                           <div class="box">
                               <img src="images/gellary/4.jpg" alt="">
                               <div class="box-content">
                                   <h3 class="title">Photo Four</h3>
                                   <ul class="icon">
                                       <li>
                                           <a href="images/gellary/4.jpg" data-lightbox="lightbox" data-title="Photo Title">
                                               <i class="fa fa-search"></i>
                                           </a>
                                       </li>
                                       <li><a href="#" class="fa fa-link"></a></li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                       <div class="col-md-4 filtr-item" data-category="3, 2" data-sort="value">
                           <div class="box">
                               <img src="images/gellary/5.jpg" alt="">
                               <div class="box-content">
                                   <h3 class="title">Photo Five</h3>
                                   <ul class="icon">
                                       <li>
                                           <a href="images/gellary/5.jpg" data-lightbox="lightbox" data-title="Photo Title">
                                               <i class="fa fa-search"></i>
                                           </a>
                                       </li>
                                       <li><a href="#" class="fa fa-link"></a></li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                       <div class="col-md-4 filtr-item" data-category="3, 1" data-sort="value">
                           <div class="box">
                               <img src="images/gellary/6.jpg" alt="">
                               <div class="box-content">
                                   <h3 class="title">Photo Six</h3>
                                   <ul class="icon">
                                       <li>
                                           <a href="images/gellary/6.jpg" data-lightbox="lightbox" data-title="Photo Title">
                                               <i class="fa fa-search"></i>
                                           </a>
                                       </li>
                                       <li><a href="#" class="fa fa-link"></a></li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
           <!-- <nav class="pagination-outer" aria-label="Page navigation">
               <ul class="pagination">
                   <li class="page-item">
                       <a href="#" class="page-link" aria-label="Previous">
                           <span aria-hidden="true">«</span>
                       </a>
                   </li>
                   <li class="page-item active"><a class="page-link" href="#">1</a></li>
                   <li class="page-item"><a class="page-link" href="#">2</a></li>
                   <li class="page-item"><a class="page-link" href="#">3</a></li>
                   <li class="page-item"><a class="page-link" href="#">4</a></li>
                   <li class="page-item"><a class="page-link" href="#">5</a></li>
                   <li class="page-item">
                       <a href="#" class="page-link" aria-label="Next">
                           <span aria-hidden="true">»</span>
                       </a>
                   </li>
               </ul>
           </nav> -->
       </div>
   </section>

   @endsection
