@extends('frontend.main')

@section('content')
<!-- Page heading Start -->
    <section class="page-heading-area jarallax overlay-black" id="water-animation">
        <img class="jarallax-img" src="images/bg/4.jpg" alt="">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="page-heading-col border-hover">
                        <h2>Contact</h2>
                        <p><a href="index-2.html">Home</a> / <a href="#">Contact</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Start -->
    <section class="contact-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="contact-col contact-infobox">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <p>yourmailone@nepalnic.com</p>
                        <p>yourmailtwo@nepalnic.com</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="contact-col contact-infobox">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <p>+0123 275 254 2458</p>
                        <p>+0123 012 001 5488</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="contact-col contact-infobox">
                      <i class="fa fa-fax" aria-hidden="true"></i>
                        <p>+1 323 555 1234</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="contact-col contact-infobox">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <p>5F-Bhatbhateni, Pulchowk, Nepal</p>
                    </div>
                </div>
            </div>
            <div class="row contact-form-row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="contact-col">
                            <div id="form-messages"></div>
                            <form id="ajax-contact" method="post" action="http://codecares.com/html/bizzhub/php/contact.php">
                                <div class="col-md-6">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Your Name" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Your Email"  required>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="subject" class="form-control" placeholder="Subject" id="subject" required>
                                </div>
                                <div class="col-md-12">
                                    <div class="contact-textarea">
                                        <textarea class="form-control" rows="6" placeholder="Your Message" id="message" name="message" required></textarea>
                                        <button class="btn btn-default theme-btn btn-hover" type="submit" value="Submit Form">Send Message</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
              </div>
              <br>
              <br>
                <div class="col-md-12">
                <div class="maps">
             <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.248574714925!2d85.31803386476733!3d27.6787111328037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19c90c823975%3A0xeff924615e0ceb61!2sBhatbhateni+Patan%2C+Sajha+Road%2C+Patan+44600%2C+Nepal!5e0!3m2!1sen!2sus!4v1529851372411" width="100%" height="450"



             frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
</div>
                <!-- <div class="col-md-12">
                    <div class="google-map-col">
                        <div id="map" style="width:100%; height:400px;"></div>
                    </div>
                </div> -->
                </section>




@endsection
