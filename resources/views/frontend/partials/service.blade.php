@extends('frontend.main')

@section('content')
<!-- Page heading Start -->
    <section class="page-heading-area jarallax overlay-black" id="water-animation">
        <img class="jarallax-img" src="images/bg/4.jpg" alt="">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="page-heading-col border-hover">
                        <h2>Our Services</h2>
                        <p><a href="index-2.html">Home</a> / <a href="#">Service</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Service Start -->
    <section class="service-two-area bg-shape">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6 fw600">
                    <div class="service-col">
                        <i class="zmdi zmdi-laptop"></i>
                        <h4><a href="#">business consulting</a></h4>
                        <p>There aremany variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                        <a class="btn btn-default theme-btn btn-hover" href="#">Read More</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6 fw600">
                    <div class="service-col">
                        <i class="zmdi zmdi-balance"></i>
                        <h4><a href="#">business investment</a></h4>
                        <p>There aremany variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                        <a class="btn btn-default theme-btn btn-hover" href="#">Read More</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6 fw600">
                    <div class="service-col">
                        <i class="zmdi zmdi-card-travel"></i>
                        <h4><a href="#">share business</a></h4>
                        <p>There aremany variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                        <a class="btn btn-default theme-btn btn-hover" href="#">Read More</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6 fw600">
                    <div class="service-col">
                        <i class="zmdi zmdi-sun"></i>
                        <h4><a href="#">business plan</a></h4>
                        <p>There aremany variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                        <a class="btn btn-default theme-btn btn-hover" href="#">Read More</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6 fw600">
                    <div class="service-col">
                        <i class="zmdi zmdi-key"></i>
                        <h4><a href="#">insurance Business</a></h4>
                        <p>There aremany variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                        <a class="btn btn-default theme-btn btn-hover" href="#">Read More</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6 fw600">
                    <div class="service-col">
                        <i class="zmdi zmdi-mall"></i>
                        <h4><a href="#">MLM Business</a></h4>
                        <p>There aremany variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                        <a class="btn btn-default theme-btn btn-hover" href="#">Read More</a>
                    </div>
                </div>
            </div>
            <!-- <nav class="pagination-outer" aria-label="Page navigation">
                <ul class="pagination">
                    <li class="page-item">
                        <a href="#" class="page-link" aria-label="Previous">
                            <span aria-hidden="true">«</span>
                        </a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item">
                        <a href="#" class="page-link" aria-label="Next">
                            <span aria-hidden="true">»</span>
                        </a>
                    </li>
                </ul>
            </nav> -->
        </div>
    </section>

@endsection
