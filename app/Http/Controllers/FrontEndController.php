<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function home(){
      return view('frontend.partials.home');
    }
public function about_us(){
  return view('frontend.partials.about-us');
}
public function contact(){
  return view('frontend.partials.contact-us');
}
public function service(){
  return view('frontend.partials.service');
}
public function media(){
  return view('frontend.partials.media');
}
}
